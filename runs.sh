#!/bin/bash
trap 'exit 130' INT
cd run
for i in {1..8}
do
    echo "Run $i..."
    ../cmake-build-debug/epinetcpp
    mkdir $i
    mv out/* $i/
    mv tally.csv $i/
done
