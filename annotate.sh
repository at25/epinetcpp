#!/bin/bash
trap 'exit 130' INT
# Annotating all PNG files in the output directory, with the data from tally.csv
LC=0
dotline='.........................'
while IFS=, read -r Time S E I R Cases
do
    if (( LC > 0 )); then
        #echo $S
        dotline='.................'
        LABEL='Susceptible'
        NUM=$(printf "%'.0f\n" "$S")
        S_LABEL=$(printf "%s%s$NUM" $LABEL "${dotline:${#NUM}}")
        dotline='.....................'
        LABEL='Exposed'
        NUM=$(printf "%'.0f\n" "$E")
        E_LABEL=$(printf "%s%s$NUM" $LABEL "${dotline:${#NUM}}")
        dotline='..................'
        LABEL='Infectious'
        NUM=$(printf "%'.0f\n" "$I")
        I_LABEL=$(printf "%s%s$NUM" $LABEL "${dotline:${#NUM}}")
        dotline='.....................'
        LABEL='Removed'
        NUM=$(printf "%'.0f\n" "$R")
        R_LABEL=$(printf "%s%s$NUM" $LABEL "${dotline:${#NUM}}")
#        echo "$R_LABEL"
        dotline='                      '
        LABEL='Propagator: EXPRADIUS + r-sampling + contact matrices'
        DTIME=$(printf "Time+%.2f" "$Time")
        DESC_LABEL=$(printf "%s%s$DTIME" "$LABEL" "${dotline:${#DTIME}}")
        #exposed=$(printf "Exposed: %'.0f\n" $E)
        ITime=${Time%.*}
        FRAME=$(printf "%04d" "$LC")
        FILENAME="out/snapshot-$ITime.png"
        echo "Converting $FILENAME..."
        convert "$FILENAME" -gamma 1.3,1.3,1.3 -scale 50% -filter Point -font ~/.fonts/i/iosevka_regular.ttf -fill white -gravity northeast -pointsize 20 -annotate +40+25 "Event-Gillespie COVID-19 spreading scenario simulation, Geneva, Switzerland" -annotate +40+50 "$DESC_LABEL" -fill lightgray -annotate +40+100 "$S_LABEL" -fill yellow -annotate +40+125 "$E_LABEL" -fill red -annotate +40+150 "$I_LABEL" -fill lightgreen -annotate +40+175 "$R_LABEL" -gravity southeast -fill lightgray -pointsize 15 -annotate +40+25 "Alexander Temerev, Liudmila Rozanova, University of Geneva, 2020" "out/c-$FRAME.png"
    fi
    (( LC+=1 ))
done < tally.csv