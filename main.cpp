#include <iostream>
#include "gdal/gdal_priv.h"
#include "gdal/gdal_utils.h"
#include "gdal/cpl_conv.h" // for CPLMalloc()
#include <string>
#include "util.h"
#include "main.h"
#include "EpiMap.h"

int main() {

    mINI::INIStructure ini = load_and_verify_config("config.ini");

    GDALAllRegister();
    GDALDataset *dataset, *mask_dataset;

    std::string& density_filename = ini["Input"]["density_file"];
    std::string& mask_filename = ini["Input"]["mask_file"];

    dataset = (GDALDataset*) GDALOpen(density_filename.c_str(), GA_ReadOnly);
    mask_dataset = (GDALDataset*) GDALOpen(mask_filename.c_str(), GA_ReadOnly);

    if (dataset != nullptr && mask_dataset != nullptr) {
        std::cout << "GDAL datasets opened!" << std::endl;

        int raster_count = dataset->GetRasterCount();
        std::cout << "Number of layers: " << raster_count << std::endl;
        if (raster_count == 0) {
            std::cerr << "No data layers found. Exiting..." << std::endl;
            std::exit(5);
        }

        int blockSizeX, blockSizeY;
        GDALRasterBand* band = dataset->GetRasterBand(1);
        band->GetBlockSize(&blockSizeX, &blockSizeY);
        printf("Block=%dx%d\n", blockSizeX, blockSizeY);

        int xSize = band->GetXSize();
        int ySize = band->GetYSize();
        auto mgr = GetGDALDriverManager();
        printf("Band=%dx%d\n", xSize, ySize);

        int x_offset = extract_int(ini, "Input", "x_offset");
        int y_offset = extract_int(ini, "Input", "y_offset");
        int width = extract_int(ini, "Input", "width");
        int height = extract_int(ini, "Input", "height");

        GDALDataset* cropped = raster_prepare(mgr, dataset, x_offset, y_offset, width, height, false);
        GDALDataset* cropped_mask = raster_prepare(mgr, mask_dataset, x_offset, y_offset, width, height, true);

        printf("Cropped to %dx%d\n", width, height);
        printf("Loading data to the model...\n");

        auto model = new EpiMap(ini, cropped, cropped_mask, mgr);

        uint32_t n = model->getN();
        printf("Number of nodes: %d\n", n);
        printf("Building the k-d tree model...\n");

        double mid_x = floor(width / 2);
        double mid_y = floor(height / 2);
        point origin = {mid_x, mid_y};

        double r = TEST_RADIUS;

        std::vector<point> result = model->radiusSearch(origin, r);
        printf("Center test: found %zu nodes!\n", result.size());

        printf("Launching simulation...\n");

        int t_max = extract_int(ini, "Simulation", "t_max");
        std::vector<double> change_points = extract_doubles(ini, "Simulation", "regime_change_days");
        std::vector<double> betas = extract_doubles(ini, "Simulation", "betas");
        std::vector<double> epsilons = extract_doubles(ini, "Simulation", "epsilons");
        std::vector<double> gammas = extract_doubles(ini, "Simulation", "gammas");
        std::vector<double> far_rates = extract_doubles(ini, "Simulation", "far_infection_rates");
        std::vector<double> local_radii = extract_doubles(ini, "Simulation", "mean_local_radii");

        std::vector<double> vaccination_times = extract_doubles(ini, "Simulation", "vaccination_times");
        std::vector<double> vaccination_rates = extract_doubles(ini, "Simulation", "vaccination_rates");
        std::vector<double> vaccination_ages = extract_doubles(ini, "Simulation", "vaccination_ages");

        int num_doses = extract_int(ini, "Simulation", "vaccination_doses");
        double interval  = extract_double(ini, "Simulation", "vaccination_interval");
        double efficiency  = extract_double(ini, "Simulation", "vaccination_efficiency");
        double detection_coeff  = extract_double(ini, "Simulation", "detection_coeff");

        double titre_decay_n50  = extract_double(ini, "Simulation", "titre_decay_n50");
        double titre_half_life  = extract_double(ini, "Simulation", "titre_half_life");

        model->simulate(t_max, change_points, betas, epsilons, gammas, far_rates, local_radii,
                        vaccination_times, vaccination_rates, vaccination_ages, num_doses, interval, efficiency,
                        detection_coeff, titre_decay_n50, titre_half_life);

        return 0;
    } else {
        std::cout << "Can't open density map or territory mask datasets "
                  << "(files exist but unreadable or wrong format?)" << std::endl;
        return 4;
    }
}

mINI::INIStructure load_and_verify_config(const std::string& config_filename) {

    if (!static_cast<bool>(std::ifstream(config_filename))) {
        std::cerr << "Error: configuration file not found (config.ini). Exiting..." << std::endl;
        std::exit(1);
    }

    mINI::INIFile file("config.ini");
    mINI::INIStructure ini;
    file.read(ini);

    std::string& density_filename = ini["Input"]["density_file"];
    std::string& mask_filename = ini["Input"]["mask_file"];

    if (!static_cast<bool>(std::ifstream(density_filename))) {
        std::cerr << "Error: density map TIF file not found: (" << density_filename << "). Exiting..." << std::endl;
        std::exit(2);
    }

    if (!static_cast<bool>(std::ifstream(mask_filename))) {
        std::cerr << "Error: territory mask file not found (" << mask_filename << "). Exiting..." << std::endl;
        std::exit(1);
    }

    return ini;
}

GDALColorTable *getColorTable() {
    auto colorTable = new GDALColorTable(GPI_RGB);

    GDALColorEntry col0, colS, colSm, colE, colEm, colI, colIm, colR, colRm, colV, colVm, colBorder, colError;
    col0.c1 = 0, col0.c2 = 0, col0.c3 = 0, col0.c4 = 255;

    colS.c1 = 99, colS.c2 = 99, colS.c3 = 99, colS.c4 = 255;
    colSm.c1 = 66, colSm.c2 = 66, colSm.c3 = 66, colSm.c4 = 255;

    colE.c1 = 248, colE.c2 = 188, colE.c3 = 20, colE.c4 = 255;
    colEm.c1 = 124, colEm.c2 = 84, colEm.c3 = 10, colEm.c4 = 255;

    colI.c1 = 248, colI.c2 = 20, colI.c3 = 20, colI.c4 = 255;
    colIm.c1 = 124, colIm.c2 = 10, colIm.c3 = 10, colIm.c4 = 255;

    colR.c1 = 20, colR.c2 = 128, colR.c3 = 20, colR.c4 = 255;
    colRm.c1 = 10, colRm.c2 = 64, colRm.c3 = 10, colRm.c4 = 255;

    colV.c1 = 20, colV.c2 = 188, colV.c3 = 248, colV.c4 = 255;
    colVm.c1 = 10, colVm.c2 = 84, colVm.c3 = 124, colVm.c4 = 255;

    colBorder.c1 = 20, colBorder.c2 = 180, colBorder.c3 = 250, colBorder.c4 = 255;
    colError.c1 = 255, colError.c2 = 0, colError.c3 = 255, colError.c4 = 255;

    for (int i = 0; i < 256; i++ ) {
        colorTable->SetColorEntry(i, &colError);
    }

    colorTable->SetColorEntry(VALUE_EMPTY, &col0);
    colorTable->SetColorEntry(VALUE_SUSCEPTIBLE, &colS);
    colorTable->SetColorEntry(VALUE_SUSCEPTIBLE_MASKED, &colSm);
    colorTable->SetColorEntry(VALUE_EXPOSED, &colE);
    colorTable->SetColorEntry(VALUE_EXPOSED_MASKED, &colEm);
    colorTable->SetColorEntry(VALUE_INFECTIOUS, &colI);
    colorTable->SetColorEntry(VALUE_INFECTIOUS_MASKED, &colIm);
    colorTable->SetColorEntry(VALUE_REMOVED, &colR);
    colorTable->SetColorEntry(VALUE_REMOVED_MASKED, &colRm);
    colorTable->SetColorEntry(VALUE_VACCINATED, &colV);
    colorTable->SetColorEntry(VALUE_VACCINATED_MASKED, &colVm);
    colorTable->SetColorEntry(VALUE_BORDER_OUT, &colBorder);

    return colorTable;
}

CPLErr applyColors(GDALDataset *dataset) {
    GDALColorTable* colorTable = getColorTable();
    return dataset->GetRasterBand(1)->SetColorTable(colorTable);
}

GDALDataset *
raster_prepare(GDALDriverManager *mgr, GDALDataset *dataset, int xOffset, int yOffset, int width, int height, bool mask) {
    GDALDriver *driverMem = mgr->GetDriverByName("MEM");
    GDALDataset* croppedRaster = driverMem->Create("", width, height, 1, GDT_Byte, nullptr);
    auto buffer = (uint8_t*) CPLMalloc(sizeof(uint8_t) * width * height);
    GDALRasterBand* inBand = dataset->GetRasterBand(1);
    GDALRasterBand* outBand = croppedRaster->GetRasterBand(1);
    rasterCopy(GF_Read, inBand, buffer, xOffset, yOffset, width, height);

    // filtering the buffer
    for (uint32_t i = 0; i < width * height; i++) {
        if (buffer[i] == VALUE_BORDER_IN) {
            if (!mask) {
                buffer[i] = VALUE_BORDER_OUT;
            }
        } else if (buffer[i] > VALUE_DENSITY_THRESHOLD) {
            buffer[i] = VALUE_SUSCEPTIBLE;
        } else {
            buffer[i] = VALUE_EMPTY;
        }
    }

    rasterCopy(GF_Write, outBand, buffer, 0, 0, width, height);
    applyColors(croppedRaster);
    CPLFree(buffer);
    return croppedRaster;
}
