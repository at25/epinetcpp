#ifndef EPINETCPP_CSVREADER_H
#define EPINETCPP_CSVREADER_H

#include <string>
#include <vector>

class CSVReader {
    std::string file_name;
    char delimiter;

public:
    explicit CSVReader(std::string fname, char delm = ',');
    std::vector<std::vector<std::string>> read_csv(int skip_lines);
};


#endif //EPINETCPP_CSVREADER_H
