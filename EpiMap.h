#ifndef EPINETCPP_EPIMAP_H
#define EPINETCPP_EPIMAP_H

#include "nanoflann.hpp"
#include "gdal/gdal_priv.h"
#include "gdal/cpl_conv.h"
#include <cstdint>
#include <vector>
#include <exception>
#include "Common.h"
#include <random>
#include <algorithm>
#include <iterator>
#include <string>
#include <iostream>
#include <fstream>
#include <queue>
#include <set>
#include "ini.h"

using namespace nanoflann;

typedef KDTreeSingleIndexAdaptor<L2_Simple_Adaptor<double, PointCloud>, PointCloud, 2> kdtree_t;

CPLErr
rasterCopy(GDALRWFlag direction, GDALRasterBand *band, uint8_t *buffer, int xOffset, int yOffset, int xSize, int ySize);

class EpiMap {

private:

    static std::mt19937 &mt() {
        // initialize once per thread
        thread_local static std::random_device srd;
        thread_local static std::mt19937 smt(srd());
        return smt;
    }

    std::uniform_real_distribution<> uniform_dist = std::uniform_real_distribution(0.0, 1.0);
    mINI::INIStructure config;

    GDALDriverManager *mgr;
    GDALDriver *driverPng;
    GDALDataset *originalDataset;
    std::map<uint32_t, uint32_t> index = {}; // coordinate index: y*width+x -> index in nodes vector
    PointCloud *point_cloud; // point cloud for nanoflann
    kdtree_t *nf_index;

    std::priority_queue<event, std::vector<event>, std::greater<>> Q = {};
    uint32_t N = 0; // nodes count
    uint32_t N_unmasked = 0; // unmasked (actually counted) nodes count
    uint8_t *buffer; // pixel buffer for direct change
    uint8_t *mask_buffer; // mask data for checking borders
    std::ofstream output; // file to write state snapshots
    state model_state = {.S = 0, .E = 0, .I = 0, .R = 0};
    std::map<int, std::vector<uint32_t>> unvaccinated; // by age group

    void dumpPng(double time);

    static double exp_variate(double v);

    void seir_expose(event event, double epsilon, double tMax);

    void seir_infect(event event, double beta, double gamma, double far_rate, double mean_local_radius, double t_max);

    void seir_remove(event event, double detection_coeff, double titre_decay_n50, double titre_half_life);

    static std::vector<int> read_pop_pyramid(std::string filename);

    static std::vector<std::vector<double>> read_double_matrix(std::string filename, int skip_lines);

    node &density_sample_node();

    bool is_masked(point loc);

    std::vector<node> find_contacts(node nd, double far_rate, double mean_local_radius);

    void make_tally(double time);

    void vaccinate(event e, const std::vector<double> &vaccination_times,
                   const std::vector<double> &vaccination_rates,
                   const std::vector<double> &vaccination_ages,
                   int vaccine_doses,
                   double vaccination_interval, double vaccine_efficiency);

    int random_unvaccinated(double min_age);

public:
    size_t width;
    size_t height;

    std::vector<node> nodes = {}; // all nodes vector (the source of truth)
    std::vector<int> pop_pyramid;
    std::vector<std::vector<double>> contact_matrix;
    std::vector<std::vector<double>> outcome_matrix;

    explicit EpiMap(mINI::INIStructure& config, GDALDataset *dataset, GDALDataset *mask, GDALDriverManager *_mgr);

    ~EpiMap();

    uint32_t getN() const;

    std::vector<point> radiusSearch(point origin, double radius);

    void simulate(double t_max, const std::vector<double> &change_points,
             const std::vector<double> &betas, const std::vector<double> &epsilons,
             const std::vector<double> &gammas, const std::vector<double> &far_rates,
             const std::vector<double> &local_radii,
             const std::vector<double> &vaccination_times,
             const std::vector<double> &vaccination_rates,
             const std::vector<double> &vaccination_ages,
             int vaccine_doses, double vaccination_interval,
             double vaccination_efficiency, double detection_coeff,
             double titre_decay_n50, double titre_half_life);

//    static std::vector<node> randomSample(size_t n, const std::vector<node> &_nodes);

    template<typename T>
    T param_function(double t, const std::vector<double> &change_points, const std::vector<T> &params);


};


#endif //EPINETCPP_EPIMAP_H
