#include "util.h"

std::string extract_string(const mINI::INIStructure &ini, const std::string &section, const std::string &key) {
    const std::string& value_str = ini.get(section).get(key);
    if (value_str.empty()) {
        std::stringstream err;
        err << "Config error: no key '" << key << "' found in section '" << section << "'" << std::endl;
        throw std::runtime_error(err.str());
    }
    std::string trim = std::regex_replace(value_str, std::regex("^ +"), "");
    trim = std::regex_replace(value_str, std::regex(" +$"), "");
    return trim;
}

double extract_double(const mINI::INIStructure &ini, const std::string &section, const std::string &key) {
    const std::string& value_str = extract_string(ini, section, key);
    double result = std::stod(value_str);
    return result;
}

std::vector<std::string> split(const std::string &s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream token_stream(s);
    while (std::getline(token_stream, token, delimiter)) {
        token = std::regex_replace(token, std::regex("^ +"), "");
        tokens.push_back(token);
    }
    return tokens;
}

std::vector<double> extract_doubles(const mINI::INIStructure &ini, const std::string &section, const std::string &key) {
    const std::string& value_str = extract_string(ini, section, key);
    std::vector<double> result;
    std::vector<std::string> tokens = split(value_str, ',');
    for (const auto& token : tokens) {
        double value_d = std::stod(token);
        result.push_back(value_d);
    }
    return result;
}

int extract_int(const mINI::INIStructure &ini, const std::string &section, const std::string &key) {
    const std::string& value_str = extract_string(ini, section, key);
    int result = std::stoi(value_str);
    return result;
}

std::vector<int> extract_ints(const mINI::INIStructure &ini, const std::string &section, const std::string &key) {
    const std::string& value_str = ini.get(section).get(key);
    std::vector<int> result;
    std::vector<std::string> tokens = split(value_str, ',');
    for (const auto& token : tokens) {
        double value_d = std::stoi(token);
        result.push_back(value_d);
    }
    return result;
}


