//
// Created by atemerev on 5/8/20.
//

#ifndef EPINETCPP_MAIN_H
#define EPINETCPP_MAIN_H

#include "ini.h"

#define TEST_RADIUS 50

mINI::INIStructure load_and_verify_config(const std::string& config_filename);

CPLErr rasterCopy(GDALRWFlag direction, GDALRasterBand* band, uint8_t* buffer, int xOffset, int yOffset, int xSize, int ySize);

GDALColorTable* getColorTable();

CPLErr applyColors(GDALDataset* dataset);

GDALDataset* raster_prepare(GDALDriverManager* mgr, GDALDataset* dataset, int xOffset, int yOffset, int width, int height, bool mask);

#endif //EPINETCPP_MAIN_H
