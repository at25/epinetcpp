//
// Created by atemerev on 6/14/20.
//

#include <fstream>
#include <sstream>
#include <utility>
#include "CSVReader.h"

CSVReader::CSVReader(std::string fname, char delm) {
    this->file_name = std::move(fname);
    this->delimiter = delm;
}

std::vector<std::vector<std::string>> CSVReader::read_csv(int skip_lines) {
    std::ifstream file(file_name);

    std::vector<std::vector<std::string>> data_list;
    std::string line;

    int skipped = 0;

    while (getline(file, line)) {
        if (skipped >= skip_lines) {
            std::vector<std::string> vec;
            std::istringstream iss(line);
            std::string token;
            while (getline(iss, token, ',')) {
                vec.emplace_back(std::string(token));
            }
            data_list.emplace_back(vec);
        } else {
            skipped++;
        }
    }

    return data_list;
}
