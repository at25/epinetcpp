import rasterio
from rasterio import features
import fiona
import fiona.transform
import pprint
import numpy as np

vec = fiona.open("/data/geo/json/switzerland.json")
ras = rasterio.open("/data/geo/switzerland.tif")

#pprint.pprint(vec[0]['geometry'])

#pprint.pprint(trans)

#print(np.max(mask))

with rasterio.open(
       '/data/geo/ch-mask.tif', 'w',
       driver='GTiff',
       transform=ras.transform,
       dtype=rasterio.uint8,
       count=1,
       width=ras.width,
       height=ras.height) as dst:
           trans = fiona.transform.transform_geom('EPSG:4326', 'EPSG:3035', vec[0]['geometry'])
           mask = features.rasterize([trans], out_shape=ras.shape, transform=ras.transform, default_value=127)
           dst.write(mask, indexes=1)

