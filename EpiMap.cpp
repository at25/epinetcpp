#include "EpiMap.h"
#include "CSVReader.h"

#include <random>
#include <string>
#include <unordered_set>
#include <numeric>
#include "util.h"

/*
std::vector<node> EpiMap::randomSample(size_t n, const std::vector<node> &_nodes) {
    size_t len = _nodes.size();
    if (n >= len || len < 100) {
        std::vector<node> result = _nodes;
        std::shuffle(result.begin(), result.end(), std::mt19937(std::random_device()()));
        result.resize(std::min(len, n));
        return result;
    } else {
        std::vector<node> sampled;
        std::unordered_set<size_t> indexes = {};
        std::uniform_int_distribution<> distr(0, (int) len - 1);

        #pragma unroll 32
        while (indexes.size() < n) {
            int r = distr(mt());
            indexes.insert(r);
        }

        for (size_t idx : indexes) {
            node node = _nodes[idx];
            sampled.push_back(node);
        }
        return sampled;
    }
}
*/

int EpiMap::random_unvaccinated(double min_age) {

    int age_group_threshold = std::floor(min_age / 5.0); // e.g. 4 -> group 0, 6 -> group 1, etc.

    // starting from the oldest age group, up to the group containing the minimum age

    std::vector<size_t> candidates;
    for (size_t i = unvaccinated.size() - 1; i >= age_group_threshold; i--) {
        if (!unvaccinated.at(i).empty()) {
            candidates.push_back(i);
        }
    }

    if (candidates.empty()) {
        return -1;
    } else {
        std::uniform_int_distribution<> idist(0, (int) candidates.size() - 1);
        int i = idist(mt());

        std::vector<uint32_t> &uv = unvaccinated.at(candidates.at(i));
        std::uniform_int_distribution<> dist(0, (int) uv.size() - 1);
        int r = dist(mt());
        int idx = (int) uv.at(r);
        std::swap(uv.at(r), uv.back());
        uv.pop_back();
        return idx;
    }
}

EpiMap::EpiMap(mINI::INIStructure &config, GDALDataset *dataset, GDALDataset *mask, GDALDriverManager *_mgr) {

    this->config = config;
    std::string pyramid_file = config.get("Input").get("pop_pyramid_file");
    std::string contact_matrix_file = config.get("Input").get("contact_matrix_file");
    std::string outcome_matrix_file = config.get("Input").get("outcome_matrix_file");
    std::cout << "Reading population pyramid..." << std::endl;
    this->pop_pyramid = EpiMap::read_pop_pyramid(pyramid_file);
    std::cout << "Reading contact matrix..." << std::endl;
    this->contact_matrix = EpiMap::read_double_matrix(contact_matrix_file, 0);
    std::cout << "Reading outcome matrix..." << std::endl;
    this->outcome_matrix = EpiMap::read_double_matrix(outcome_matrix_file, 1);

    this->output = std::ofstream("tally.csv");
    this->output << "Time,S,E,I,R,Cases,V,H,D,HD";

    #pragma unroll 16
    for (int i = 0; i < 16; i++) {
        this->output << ",s" << i * 5;
    }

    #pragma unroll 16
    for (int i = 0; i < 16; i++) {
        this->output << ",e" << i * 5;
    }

    #pragma unroll 16
    for (int i = 0; i < 16; i++) {
        this->output << ",i" << i * 5;
    }

    #pragma unroll 16
    for (int i = 0; i < 16; i++) {
        this->output << ",r" << i * 5;
    }

    #pragma unroll 16
    for (int i = 0; i < 16; i++) {
        this->output << ",v" << i * 5;
    }

    #pragma unroll 16
    for (int i = 0; i < 16; i++) {
        this->output << ",h" << i * 5;
    }

    #pragma unroll 16
    for (int i = 0; i < 16; i++) {
        this->output << ",d" << i * 5;
    }

    #pragma unroll 16
    for (int i = 0; i < 16; i++) {
        this->output << ",hd" << i * 5;
    }

    this->output << std::endl;

    mt();
    this->mgr = _mgr;
    this->originalDataset = dataset;
    this->driverPng = mgr->GetDriverByName("PNG");
    GDALRasterBand *band = dataset->GetRasterBand(1);
    this->width = band->GetXSize();
    this->height = band->GetYSize();

    this->buffer = (uint8_t *) CPLMalloc(sizeof(uint8_t) * width * height);
    this->mask_buffer = (uint8_t *) CPLMalloc(sizeof(uint8_t) * width * height);
    this->point_cloud = new PointCloud;

    std::cout << "Allocating rasters..." << std::endl;

    CPLErr err = rasterCopy(GF_Read, band, buffer, 0, 0, width, height);
    if (err) {
        throw std::runtime_error("Cannot allocate main raster data");
    }
    err = rasterCopy(GF_Read, mask->GetRasterBand(1), mask_buffer, 0, 0, width, height);
    if (err) {
        throw std::runtime_error("Cannot allocate mask raster data");
    }

    std::random_device rd;
    std::mt19937 gen(rd());
    std::discrete_distribution<> ddist(this->pop_pyramid.begin(), this->pop_pyramid.end());
    std::uniform_real_distribution<> udist(0., 1.);

    std::cout << "Counting population..." << std::endl;
    int node_count = 0;

    for (uint32_t i = 0; i < height; i++) {
        for (uint32_t j = 0; j < width; j++) {
            uint32_t idx = i * width + j;
            if (buffer[idx] == VALUE_SUSCEPTIBLE) {
                auto x = static_cast<double>(j);
                auto y = static_cast<double>(i);
                point loc = {x, y};
                if (!is_masked(loc)) {
                    node_count++;
                }
            }
        }
    }
    std::string population_s = config.get("Simulation").get("population");
    int population = std::stoi(population_s);
    double density_rate = static_cast<double>(population) / static_cast<double>(node_count);
    std::cout << "Density rate: " << density_rate << std::endl;

    std::cout << "Preprocessing raster..." << std::endl;

    // first, prepopulate the unvaccinated map

    for (int i = 0; i < 16; i++) {
        std::vector<uint32_t> empty;
        unvaccinated[i] = empty;
    }

    for (uint32_t i = 0; i < height; i++) {
        for (uint32_t j = 0; j < width; j++) {
            uint32_t idx = i * width + j;
            double rnd = udist(mt());
            if (buffer[idx] == VALUE_SUSCEPTIBLE) {
                if (rnd < density_rate) {
                    // append the node to the vector of all nodes
                    uint8_t age_group = ddist(gen);
                    node n = {N, 'S', static_cast<double>(j), static_cast<double>(i), age_group, false, NoOutcomeYet,
                              Undetermined, 1.0, 0, 0};

                    if (!this->is_masked(n.loc)) {
                        N_unmasked++;
                        n.included = true;
                    } else {
                        buffer[idx] = VALUE_SUSCEPTIBLE_MASKED;
                        n.included = false;
                    }
                    // adding node everywhere
                    nodes.push_back(n);
                    point pt = {.x = static_cast<double>(j), .y = static_cast<double>(i)};
                    index.insert({idx, N});
                    point_cloud->pts.emplace_back(pt);
                    unvaccinated[age_group].push_back(N);
                    N++;
                } else {
                    buffer[idx] = VALUE_EMPTY;
                }
            }
        }
    }

    std::cout << "Raster loaded: " << node_count << " nodes; population " << N_unmasked << ", ref population: " << population
              << "." << std::endl;

    std::cout << "Unvaccinated counts: ";
    for (int j = 0; j < this->unvaccinated.size(); j++) {
        std::cout << this->unvaccinated.at(j).size() << " ";
    }
    std::cout << std::endl;


    this->nf_index = new kdtree_t(2, *this->point_cloud, KDTreeSingleIndexAdaptorParams(10));
    this->nf_index->buildIndex();

    this->model_state = {
            .S = N_unmasked, .E = 0, .I = 0, .R = 0
    };
}

EpiMap::~EpiMap() {
    CPLFree(this->buffer);
    this->output.close();
    delete this->point_cloud;
    delete this->nf_index;
}

uint32_t EpiMap::getN() const {
    return N;
}

std::vector<point> EpiMap::radiusSearch(point origin, double radius) {
    double r_squared = radius * radius;
    std::vector<std::pair<size_t, double>> ret_matches;
    nanoflann::SearchParams params;
    params.sorted = false;
    const double query_pt[2] = {origin.x, origin.y};
    const size_t n_matches = this->nf_index->radiusSearch(&query_pt[0], r_squared, ret_matches, params);
    std::vector<point> result;
    for (size_t i = 0; i < n_matches; i++) {
        size_t idx = ret_matches[i].first;
        point p = this->point_cloud->pts.at(idx);
        result.emplace_back(p);
    }
    std::shuffle(result.begin(), result.end(), mt());
    return result;
}

void EpiMap::simulate(double t_max, const std::vector<double> &change_points,
                      const std::vector<double> &betas, const std::vector<double> &epsilons,
                      const std::vector<double> &gammas, const std::vector<double> &far_rates,
                      const std::vector<double> &local_radii,
                      const std::vector<double> &vaccination_times,
                      const std::vector<double> &vaccination_rates,
                      const std::vector<double> &vaccination_ages,
                      int vaccine_doses, double vaccination_interval,
                      double vaccination_efficiency,
                      double detection_coeff,
                      double titre_decay_n50,
                      double titre_half_life) {
//    std::vector<node> infected = randomSample(2, nodes);

    int x = extract_int(config, "Input", "start_x");
    int y = extract_int(config, "Input", "start_y");
    int r = extract_int(config, "Input", "start_r");
    int initial_infected = extract_int(config, "Input", "initial_infected");
    point origin = {static_cast<double>(x), static_cast<double>(y)};
    std::vector<point> result = radiusSearch(origin, r);
    std::shuffle(result.begin(), result.end(), mt());
    int size = static_cast<int>(result.size());

    // assigning initial infected

    for (int i = 0; i < std::min(initial_infected, size); i++) {
        point p = result.at(i);
        uint32_t idx = (uint32_t) p.y * width + p.x;
        uint32_t num = index[idx];
        std::cout << "Initial infected node " << num << ", x: " << p.x << ", y: " << p.y << std::endl;
        event event = {0, num, Infect};
        Q.push(event);
    }

    // pushing initial dump event

    event initial_dump = {0, 0, Dump};
    Q.push(initial_dump);

    // pushing initial vaccination event

    if (!vaccination_times.empty() && vaccination_times[0] >= 0) {
        double t0 = vaccination_times[0];
        int vacc_age = vaccination_ages.at(1);
        uint32_t num = random_unvaccinated(vacc_age);
        event first_vaccination = {t0, num, Vaccinate};
        Q.push(first_vaccination);
    }

    // main loop

    while (!Q.empty()) {
        event e = Q.top();
        if (e.time < t_max) {
            double epsilon, beta, gamma, far_rate, local_radius;
            switch (e.action) {
                case Expose:
                    epsilon = param_function(e.time, change_points, epsilons);
                    seir_expose(e, epsilon, t_max);
                    break;
                case Infect:
                    beta = param_function(e.time, change_points, betas);
                    gamma = param_function(e.time, change_points, gammas);
                    far_rate = param_function(e.time, change_points, far_rates);
                    local_radius = param_function(e.time, change_points, local_radii);
                    seir_infect(e, beta, gamma, far_rate, local_radius, t_max);
                    break;
                case Remove:
                    seir_remove(e, detection_coeff, titre_decay_n50, titre_half_life);
                    break;
                case Dump:
                    this->make_tally(e.time);
                    if (Q.size() > 1) {
                        event dump_event = {e.time + 1, 0, Dump};
                        Q.push(dump_event);
                    }
                    break;
                case Vaccinate:
                    this->vaccinate(e, vaccination_times, vaccination_rates, vaccination_ages, vaccine_doses,
                                    vaccination_interval, vaccination_efficiency);
                    break;
                default:
                    std::cerr << "Unknown action: " << e.action << std::endl;
            }
            Q.pop();
        } else {
            break;
        }
    }
}

void EpiMap::vaccinate(event e, const std::vector<double> &vaccination_times,
                       const std::vector<double> &vaccination_rates,
                       const std::vector<double> &vaccination_ages,
                       int vaccine_doses, double vaccination_interval, double vaccination_efficiency) {

    node &target = nodes.at(e.node_index); // a reference

    if (target.vaccinations == 0 ) {

        // first vaccination:
        // Proceeding with the next vaccination event for some other node

        double min_age = param_function(e.time, vaccination_times, vaccination_ages);
        int idx_next = random_unvaccinated(min_age); // todo support density targeting strategy

        if (idx_next != -1) {
            node &next = nodes.at(idx_next);
            double rate = param_function(e.time, vaccination_times, vaccination_rates);
            double delay = 1.0 / rate;
            double next_time = e.time + delay;
            auto v = event{next_time, next.index, Vaccinate};
            Q.push(v);
        } else {
            std::cout << "Final vaccination?" << std::endl;
        }

        target.susceptibility = target.susceptibility * (1 - vaccination_efficiency);
        target.vaccinations++;
        target.last_vaccinated = e.time;

        // marking the pixel in the buffer as vaccinated

        bool is_masked = this->is_masked(target.loc);
        uint32_t buffer_index = target.loc.y * width + target.loc.x;
        this->buffer[buffer_index] = is_masked ? VALUE_VACCINATED_MASKED : VALUE_VACCINATED;

        // Sending the subsequent vaccination event to the future

        if (vaccine_doses > 1) {
            event next_dose = {e.time + vaccination_interval, e.node_index, Vaccinate};
            Q.push(next_dose);
        }

    } else {
        // Subsequent vaccination

        target.susceptibility = target.susceptibility * (1 - vaccination_efficiency);
        target.vaccinations++;
        target.last_vaccinated = e.time;

        if (target.vaccinations < vaccine_doses) {
            event next_dose = {e.time + vaccination_interval, e.node_index, Vaccinate};
            Q.push(next_dose);
        }
    }
    if (target.vaccinations > vaccine_doses) {
        std::cerr << "ERROR: Overvaccinated? " << target.index << std::endl;
    }
}

void EpiMap::make_tally(double time) {
    uint32_t age_s[16] = {0}, age_e[16] = {0}, age_i[16] = {0}, age_r[16] = {0}, age_v[16] = {0}, age_h[16] = {0}, age_d[16] = {0}, age_hd[16] = {0};
    uint32_t s = 0, e = 0, i = 0, r = 0, v = 0, h = 0, d = 0, hd = 0;
    for (auto node : this->nodes) {
        if (node.included) {
            if (node.status == 'S') {
                s++;
                age_s[node.age_group]++;
            } else if (node.status == 'E') {
                e++;
                age_e[node.age_group]++;
            } else if (node.status == 'I') {
                i++;
                age_i[node.age_group]++;
            } else if (node.status == 'R') {
                r++;
                age_r[node.age_group]++;
            }
            if (node.vaccinations > 0) {
                v++;
                age_v[node.age_group]++;
            }
            if (node.substate == Hospitalized) {
                h++;
                age_h[node.age_group]++;
                if (node.outcome == Dead) {
                    hd++;
                    age_hd[node.age_group]++;
                }
            }
            if (node.outcome == Dead) {
                d++;
                age_d[node.age_group]++;
            }
        }
    }
    this->output << time << "," << s << "," << e << "," << i << "," << r << ","
                 << i + r << "," << v << "," << h << "," << d << "," << hd;
    for (uint32_t as : age_s) {
        this->output << "," << as;
    }
    for (uint32_t ae : age_e) {
        this->output << "," << ae;
    }
    for (uint32_t ai : age_i) {
        this->output << "," << ai;
    }
    for (uint32_t ar : age_r) {
        this->output << "," << ar;
    }
    for (uint32_t av : age_v) {
        this->output << "," << av;
    }
    for (uint32_t ah : age_h) {
        this->output << "," << ah;
    }
    for (uint32_t ad : age_d) {
        this->output << "," << ad;
    }
    for (uint32_t ahd : age_hd) {
        this->output << "," << ahd;
    }
    this->output << std::endl;
    std::cout << "Time " << time << "..." << std::endl;
//    this->dumpPng(time);
    std::cout << "Unvaccinated counts: ";
    for (int j = 0; j < this->unvaccinated.size(); j++) {
        std::cout << this->unvaccinated.at(j).size() << " ";
    }
    std::cout << std::endl;
}

void EpiMap::seir_expose(event e, double epsilon, double tMax) {
//    std::cout << "Expose" << std::endl;
    node &node = nodes.at(e.node_index); // a reference
    if (node.status == 'S') { // only susceptible nodes are exposed
        node.status = 'E';
        // update the result tallies
        bool is_masked = this->is_masked(node.loc);

        if (!is_masked) {
            this->model_state.S--;
            this->model_state.E++;
        }

        uint32_t buffer_index = node.loc.y * width + node.loc.x;
        this->buffer[buffer_index] = is_masked ? VALUE_EXPOSED_MASKED : VALUE_EXPOSED;

        double inf_time = e.time + exp_variate(epsilon);
        if (inf_time < tMax) {
            event inf_event = {inf_time, node.index, Infect};
            Q.push(inf_event);
        }
    }
}

void EpiMap::seir_infect(event e, double beta, double gamma, double far_rate, double mean_local_radius, double t_max) {
//    std::cout << "Infect" << std::endl;
    node &nd = nodes.at(e.node_index); // a reference
    if (nd.status == 'E' || nd.status == 'S') { // only exposed nodes can be infected

        // update the result tallies
        bool is_masked = this->is_masked(nd.loc);
        if (!is_masked) {
            if (nd.status == 'E') { this->model_state.E--; } else if (nd.status == 'S') { this->model_state.S--; }
            this->model_state.I++;
        }

        nd.status = 'I';
        uint32_t buffer_index = nd.loc.y * width + nd.loc.x;
        this->buffer[buffer_index] = is_masked ? VALUE_INFECTIOUS_MASKED : VALUE_INFECTIOUS;

        // inserting removal event in the future
        double rem_time = e.time + exp_variate(gamma);
        if (rem_time < t_max) {
            event rem_event = {rem_time, nd.index, Remove};
            Q.push(rem_event);
        }

        double next_event_time = e.time;
        double dt = exp_variate(beta);
        next_event_time += dt;

        while (next_event_time < rem_time) {
            std::vector<node> contacts = find_contacts(nd, far_rate, mean_local_radius); // 0 or 1 todo optimize
            double rnd = uniform_dist(mt());

            #pragma unroll 1
            for (node u : contacts) { // either 0 nodes or 1 node, always
//                if (u.status == 'S' && rnd < u.susceptibility) {
                if (u.status == 'S') {
                    event exp_event = {next_event_time, u.index, Expose};
                    Q.push(exp_event);
                }
            }

            dt = exp_variate(beta);
            next_event_time += dt;
        }
    }
}

void EpiMap::seir_remove(event e, double detection_coeff, double titre_decay_n50, double titre_half_life) {
//    std::cout << "Remove" << std::endl;
    node &nd = nodes.at(e.node_index); // a reference
    if (nd.status == 'I') { // only infected nodes can be removed
        nd.status = 'R';

        // setting the outcome according to the outcome matrix

        std::vector<double> outcomes = this->outcome_matrix.at(nd.age_group);
        outcomes.at(1) /= detection_coeff; // most of recovered-unhospitalized cases are undetected
        // here, we will model the action of vaccines as a corresponding increase in the number of recoveries
        // todo: here, recalculate susceptibility as falling with time

        // calculate current titre, knowing the time, half-life and n_50:
        double lambda = std::log(2) / titre_half_life;
        double t = nd.last_vaccinated == 0 ? 0 : e.time - nd.last_vaccinated;
        double titre = std::exp(-lambda * t);

        // now, calculate the current k we operate under:
        double k = -std::log(1 / (1 - nd.susceptibility) - 1) / (1 - titre_decay_n50);

        // and finally, obtain the real "protection rate" knowing the current titre and k:
        double protection = 1 / (1 + std::exp(-k * (titre - titre_decay_n50)));

        if (protection >= 1 || protection < 0) {
            std::cerr << "ERROR ERROR protection wrong" << std::endl;
            exit(127);
        }

/*
        if (protection != 0) {
            std::cout << "Protection: " << protection << std::endl;
        }
*/

        outcomes.at(1) /= (1 - protection); // vaccines decrease both deaths and hospitalizations

        auto hist = std::discrete_distribution<int>(outcomes.begin(), outcomes.end());
        int outcome_index = hist(mt());

        switch (outcome_index) {
            case 0: // hospitalized, recovered
                nd.substate = Hospitalized;
                nd.outcome = Recovered;
                break;
            case 1: // not hospitalized, recovered
                nd.substate = Undetermined;
                nd.outcome = Recovered;
                break;
            case 2: // hospitalized, deceased
                nd.substate = Hospitalized;
                nd.outcome = Dead;
                break;
            case 3: // not hospitalized, deceased
                nd.substate = Undetermined;
                nd.outcome = Dead;
                break;
            default:
                std::cerr << "Unexpected outcome index: " << outcome_index << std::endl;
                throw std::exception();
        }

        // update the result tallies
        bool is_masked = this->is_masked(nd.loc);
        if (!is_masked) {
            this->model_state.I--;
            this->model_state.R++;
        }

        uint32_t buffer_index = nd.loc.y * width + nd.loc.x;
        this->buffer[buffer_index] = is_masked ? VALUE_REMOVED_MASKED : VALUE_REMOVED;
    } else {
        std::cerr << "Unexpected status for node " << nd.index << ": " << nd.status << std::endl;
        throw std::exception();
    }
}

std::vector<node> EpiMap::find_contacts(node nd, double far_rate, double mean_local_radius) {

    std::vector<double> contact_row = this->contact_matrix.at(nd.age_group);
    auto hist = std::discrete_distribution<int>(contact_row.begin(), contact_row.end());
    int dest_group = hist(mt());
    double rnd = this->uniform_dist(mt());

    if (rnd < far_rate) { // far contact
        for (int i = 0; i < 5000; i++) { // todo config limit
            node n = density_sample_node();
            if (n.age_group == dest_group) {
                std::vector<node> result;
                result.emplace_back(n);
                return result;
            }
        }
    } else { // near contact
        double radius = exp_variate(1 / mean_local_radius);
        point origin = nd.loc;
        std::vector<point> contact_coords = radiusSearch(origin, radius);
        int until = std::min<int>(contact_coords.size(), 5000); // todo config
        for (int i = 0; i < until; i++) {
            point p = contact_coords.at(i);
            uint32_t p_idx = std::lround(p.y) * this->width + std::lround(p.x);
            uint32_t n_idx = index.at(p_idx);
            if (n_idx > nodes.size()) {
                throw std::runtime_error("Oops1");
            }
            node &n = nodes.at(n_idx);
            if (n.age_group == dest_group) {
                std::vector<node> result;
                result.emplace_back(n);
                return result;
            }
        }
    }

    return {};
}

double EpiMap::exp_variate(double v) {
    std::uniform_real_distribution<double> distr(0, 1);
    double u = distr(mt());
    return -std::log(u) / v;
}

void EpiMap::dumpPng(double time) {
    char fileName[100];
    int ts = static_cast<int>(time);
    sprintf(fileName, "./out/snapshot-%d.png", ts);
    rasterCopy(GF_Write, originalDataset->GetRasterBand(1), this->buffer, 0, 0, width, height);
    GDALDataset *pngDs = this->driverPng->CreateCopy(fileName, this->originalDataset, FALSE, nullptr, nullptr, nullptr);
    GDALFlushCache(pngDs);
    GDALClose(pngDs);
}

std::vector<int> EpiMap::read_pop_pyramid(std::string filename) {
    CSVReader reader(std::move(filename), ',');
    std::vector<int> result;
    std::vector<std::vector<std::string>> strings = reader.read_csv(0);
    int rc = 0;
    for (std::vector<std::string> row: strings) {
        if (rc != 0) {
            int maleCount = std::stoi(row[1]);
            int femaleCount = std::stoi(row[2]);
            int total = maleCount + femaleCount;
            result.emplace_back(total);
        }
        rc++;
    }
    return result;
}

std::vector<std::vector<double>> EpiMap::read_double_matrix(std::string filename, int skip_lines) {
    CSVReader reader(std::move(filename), ',');
    std::vector<std::vector<double>> result;
    std::vector<std::vector<std::string>> strings = reader.read_csv(skip_lines);
    for (const std::vector<std::string> &row: strings) {
        std::vector<double> vec;
        for (const std::string &str : row) {
            vec.emplace_back(std::stod(str));
        }
        result.emplace_back(vec);
    }
    return result;
}

node &EpiMap::density_sample_node() {
    std::uniform_int_distribution<> distr(0, width * height - 1);
    size_t pointLoc = distr(mt());
    auto search = index.find(pointLoc);
    if (search != index.end()) {
        size_t idx = search->second;
        if (idx > nodes.size()) {
            throw std::runtime_error("Oops2");
        }
        node &nd = nodes.at(idx);
        return nd;
    } else {
        return density_sample_node();
    }
}

CPLErr
rasterCopy(GDALRWFlag direction, GDALRasterBand *band, uint8_t *buffer, int xOffset, int yOffset, int xSize,
           int ySize) {
    return band->RasterIO(direction, xOffset, yOffset, xSize, ySize, buffer, xSize, ySize, GDT_Byte, 0, 0);
}

bool EpiMap::is_masked(point loc) {
    uint32_t buffer_index = loc.y * width + loc.x;
    uint8_t maskData = this->mask_buffer[buffer_index];
    return maskData != VALUE_MASK;
}

template<typename T>
T EpiMap::param_function(double time, const std::vector<double> &change_points, const std::vector<T> &params) {
    if (change_points.size() + 1 != params.size()) {
        throw std::invalid_argument("Params size should be equal to change points size + 1");
    }
    for (int i = 0; i < change_points.size(); i++) {
        if (time < change_points[i]) {
            return params[i];
        }
    }
    return params[params.size() - 1];
}
