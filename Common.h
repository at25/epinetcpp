#ifndef EPINETCPP_COMMON_H
#define EPINETCPP_COMMON_H

#include <cstdint>

enum Action {Expose, Infect, Remove, Dump, Vaccinate, ChangeSubstate};

enum Outcome {NoOutcomeYet, Dead, Recovered};
enum Substate {Undetermined, Hospitalized, Discharged};

enum VaccinationStrategy {Random, Density};

struct point {
    double x;
    double y;
};

struct state {
    uint32_t S;
    uint32_t E;
    uint32_t I;
    uint32_t R;
};

struct node {
    uint32_t index;
    char status;
    point loc;
    uint8_t age_group;
    bool included;
    Outcome outcome;
    Substate substate;
    double susceptibility;
    int vaccinations;
    double last_vaccinated;
};

struct event {
    double time;
    uint32_t node_index;
    Action action;
    bool push_next = true;

    bool operator >(const event& other) const {
        return (time > other.time);
    }
};

struct PointCloud {

    std::vector<point> pts;

    // Must return the number of data points
    inline size_t kdtree_get_point_count() const { return pts.size(); }

    // Returns the dim'th component of the idx'th point in the class:
    // Since this is inlined and the "dim" argument is typically an immediate value, the
    //  "if/else's" are actually solved at compile time.
    inline double kdtree_get_pt(const size_t idx, const size_t dim) const {
        if (dim == 0) { return pts[idx].x; } else { return pts[idx].y; }
    }

    // Optional bounding-box computation: return false to default to a standard bbox computation loop.
    //   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
    //   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
    template <class BBOX>
    bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }

};

#define VALUE_EMPTY 0
#define VALUE_BORDER_IN 127
#define VALUE_BORDER_OUT 255
#define VALUE_MASK 127

#define VALUE_SUSCEPTIBLE 1
#define VALUE_SUSCEPTIBLE_MASKED 2
#define VALUE_EXPOSED 64
#define VALUE_EXPOSED_MASKED 65
#define VALUE_INFECTIOUS 128
#define VALUE_INFECTIOUS_MASKED 129
#define VALUE_REMOVED 192
#define VALUE_REMOVED_MASKED 193
#define VALUE_VACCINATED 195
#define VALUE_VACCINATED_MASKED 196

#define VALUE_DENSITY_THRESHOLD 100

#endif //EPINETCPP_COMMON_H
