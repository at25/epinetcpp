#ifndef EPINETCPP_UTIL_H
#define EPINETCPP_UTIL_H

#include <vector>
#include <string>
#include <sstream>
#include <regex>
#include "ini.h"

std::string extract_string(const mINI::INIStructure& ini, const std::string& section, const std::string& key);

double extract_double(const mINI::INIStructure& ini, const std::string& section, const std::string& key);

std::vector<std::string> split(const std::string& s, char delimiter);

std::vector<double> extract_doubles(const mINI::INIStructure& ini, const std::string& section, const std::string& key);

int extract_int(const mINI::INIStructure& ini, const std::string& section, const std::string& key);

std::vector<int> extract_ints(const mINI::INIStructure& ini, const std::string& section, const std::string& key);

#endif //EPINETCPP_UTIL_H